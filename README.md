GitLab Workflow 練習區
=======================

目的
----
模擬影片中的流程與事項，請觀看本影片：https://about.gitlab.com/2017/03/22/gitlab-9-0-released/ 

學習 GitLab Workflow, 透過修改 GitLab Page 來模擬團隊運作，每一個人可以是 PM/DEV/Review/OPS 的角色。
 

專案資訊
---------
專案名稱: gitlab-class  
專案位置: git@gitlab.com:haway-test/gitlab-class.git  
slack: https://gitlab-class.slack.com  
共筆: http://gitlab-class-md.rsync.tw (https://hackmd.io/IYMwbAJmCcYMwFowCNoAYEBY0FNMOgCZgMJCBWaCHZZAdmAGNMg=)
本專案的 Page: https://gitlab.rsync.tw

GitaLab 已經跟 slack 接起來，你可以在 slack 中輸入 /gitlab [help] 取得指令

專案內容: 

一個 GitLab Page 網頁，可以用 https://gitlab.rsync.tw 或 https://haway-test.gitlab.io/gitlab-class 連接
SSL 使用 let's encrypt


參與人員
--------
你可以指派 Issus 給這些人

1. haway  
2. [凍仁翔 (Chu-Siang Lai)](https://gitlab.com/chusiang)
3. AdanLin

練習項目
--------
- 在 Slack 或是 gitlab 上面開開 Issus
- Issus 可以放者不管，或是練習將 Issus 指派給其他人，被指派的人不一定要完成，以練習為目的
- 若是想自己練習 commit 時關閉 issus 或是練習發 PR，那就請自己去撿 Issue。
- 修改程式碼，push 到 GitLab 的分支，並且對應 Issue 號碼，發起 PR
- Code Review (目前不確定這部份在 GitLab 怎麼做)
- 合併到 Master 分支
- 從 Slack 上面進行佈署
- 觀看 CI/CD 的結果
- 檢視 GitLab Page 的修改
- 其他奇淫技巧
- 你可以任意修改本專案的任何東西，包括 gitlab-ci.yml 或是 README.md 記得在開一個 issue 紀錄一下
- 你可以佈署或是與其他東西搭配，譬如 Docker / Ansible / Anything...

加入方式
========
- 如果你想，先加入 Slack，不勉強
- 你必須有 GitLab 帳號
- 也可以用 git clone 的方式複製專案到你本地機器，或是 fork 到你的 GitLab


其他事項
========
- Git Log 中英文不拘，但請盡量撰寫一些內容
- 線上不一定有人，請勿要求別人一定要配合你的作法，可以先在 Slack 上面發問
- 你不用真的完成 Issue 的要求，你可以隨便修改內容然後說 close #issue，當然你也可以真的解決 Issue 的內容
- 如想要介接其他平台或功能，譬如 Docker 或是 Ansible 也可以，需要我配合修改設定請與我聯絡(在 Slack 上發 Private Message 給我)


加入專案之後不知道要幹麻？你可以....
=====================================
- 開 Issue
- Fork/Clone 專案到你的家目錄
- 看看有哪些 Issue 需要解決
- 嘗試修改 .gitlab-ci.yml 或是任何檔案

